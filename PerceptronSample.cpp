// PerceptronSample.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"

#define ITEM_COUNT 6

double vector_dev(double* m1, double* m2)
{
	int i;
	double result = 0;
	for (i = 0; i < 2; i++)  //行 2=2次元
	{
		result += m1[i] * m2[i];
	}
	return result;
}

int main()
{
	int i;
	double rate = 2; //学習係数（ρ）
	int correct = 0; //正解数
	int edu_times = 0; //学習回数

	double arr[] = { -1.5, -1.0, -0.5, -0.2, 0.2, 1.2 };
	int label[] = { -1,   -1,   -1,    1,   1,   1 }; //-1=クラス1 1=クラス2

	double edu[ITEM_COUNT][2];

	for (int i = 0; i < ITEM_COUNT; i++)
	{
		edu[i][0] = 1.0;
		edu[i][1] = arr[i];
	}

	// 重みベクトル(w)
	double w[] = { 0.0, 0.0 };

	// 学習開始 
	while (correct < ITEM_COUNT)
	{
		correct = 0;

		for (i = 0; i < ITEM_COUNT; i++) {
			if (vector_dev(w, edu[i]) > 0.0 && label[i] == 1)
			{
				correct += 1;
			}
			else if (vector_dev(w, edu[i]) <= 0 && label[i] == -1)
			{
				correct += 1;
			}
			else
			{
				w[0] = w[0] + rate * edu[i][0] * (double)label[i];
				w[1] = w[1] + rate * edu[i][1] * (double)label[i];

				printf("識別関数： g(x) = %.0f * x + (%.0f)\n", w[1], w[0]);
				edu_times++;
			}
		}
	}

	printf("学習回数： %d回\n", edu_times);
	printf("Press Enter Key To Exit\n");
	scanf("");
	return 0;
}

